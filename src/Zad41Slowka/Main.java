package Zad41Slowka;

//1. Coraz większą popularność wśród mło zyskuje stara gra w słówka.  Polega ona na tym, że pierwszy gracz mówi wybrane
// przez siebie słowo a gracz drugi musi podać słowo rozpoczynające się na literę ostatnią ze słowa pierwszego gracza.
//        Posiadając listę różnych wyrazów (inną dla każdego z graczy), sprawdź który z graczy (pierwszy czy drugi)
// wygra pojedynek w słówka.
//        Wybierz losowo pierwszą literę ze wszytkich wyrazów gracza 1. Następnie sprawdź kto wygra.
//        DODATKOWO: Wyświetl wyrazy jakie powi gracz 1 oraz gracz 2

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {

        Scanner scfile1 = new Scanner(new File("file1"));
        Scanner scfile2 = new Scanner(new File("file2"));
        List<String> firstPlayer = new ArrayList<>();
        List<String> secondPlayer = new ArrayList<>();

        while (scfile1.hasNext()){
            firstPlayer.add(scfile1.next());

        }
        scfile1.close();
        while (scfile2.hasNext()){
            secondPlayer.add(scfile2.next());

        }
        scfile2.close();
        System.out.println(firstPlayer);

        Play play = new Play(firstPlayer, secondPlayer);
        play.letsGo();
    }
}
